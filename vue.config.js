module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "~@/styles/_basis.scss";'
      }
    }
  },
  configureWebpack: {
    devtool: 'source-map'
  }
}
