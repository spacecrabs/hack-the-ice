import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './styles/main.scss'

import VueApollo from 'vue-apollo'
import ApolloClient from '@/plugins/ApolloClient/'

Vue.use(VueApollo)

Vue.config.productionTip = false

const apolloProvider = new VueApollo({
  defaultClient: ApolloClient
})

new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount('#app')
