import ru from 'date-fns/locale/ru'

import format from 'date-fns/format'
import isValid from 'date-fns/isValid'

/**
 * Отформатировать дату (дата - дата)
 * @param {Date} date дата
 * @param {String} token вид
 * @param {String} empty суффикс
 */
export function formatDate (date, token, empty = '') {
  if (!date || date === '0000-00-00' || !isValid(date)) return empty

  return format(date, token, { locale: ru })
}

/**
 * Проверка инн
 * @param {Number} инн
 */
export function isInnValid (inn) {
  inn = String(inn).replace(/[^0-9]+/g, '').split('')
  if (inn.length === 10) {
    return inn[9] === String(((
      2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
      3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
      4 * inn[6] + 6 * inn[7] + 8 * inn[8]
    ) % 11) % 10)
  } else if (inn.length === 12) {
    return inn[10] === String(((
      7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
      10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
       9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
       8 * inn[9]
    ) % 11) % 10) && inn[11] === String(((
      3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
      4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
      5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
      6 * inn[9] + 8 * inn[10]
    ) % 11) % 10)
  }
  return false
}

/**
 * Проверка ОГРН и ОГРНИП
 * @param {Number} ОГРН или ОГРНИП
 */
export function isOgrnValid (ogrn) {
  ogrn = String(ogrn).replace(/[^0-9]+/g, '')
  if (ogrn.length === 13) {
    // ОГРН
    return ogrn[12] === (parseInt(ogrn.slice(0, -1)) % 11).toString().slice(-1)
  } else if (ogrn.length === 15) {
    // ОГРНИП
    return ogrn[14] === (parseInt(ogrn.slice(0, -1)) % 13).toString().slice(-1)
  }
  return false
}
