import Vue from 'vue'
import VueRouter from 'vue-router'

import PagesRoutes from '@/modules/pages/router/'
import SignupRoutes from '@/modules/signup/router/'
import AccountRoutes from '@/modules/account/router/'
import CatalogRoutes from '@/modules/catalog/router/'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    ...PagesRoutes,
    ...SignupRoutes,
    ...AccountRoutes,
    ...CatalogRoutes,
    {
      path: '*',
      redirect: {
        name: 'not-found'
      }
    }
  ]
})

export default router
