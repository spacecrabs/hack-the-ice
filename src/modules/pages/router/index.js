export default [
  {
    path: '/',
    name: 'home',
    redirect: {
      name: 'signup-search'
    }
  },
  {
    path: '/404',
    name: 'not-found',
    component: () => import(/* webpackChunkName: "not-found" */ '../views/NotFound')
  }
]
