import gql from 'graphql-tag'

export const USER_INSERT = gql`
  mutation userInsert($object: [user_insert_input!]!) {
    insert_user(objects: $object) {
      returning {
        id
      }
    }
  }
`

export const USER = gql`
  query userOnSignup($id: Int!)  {
    user: user_by_pk(id: $id) {
      id
      inn
      company_info {
        id
        company_name
        rating
      }
    }
  }
`

export const SERVICE_LIST = gql`
  query serviceList {
    serviceList: service {
      id
      title
    }
  }
`
