export default [
  {
    path: '/signup',
    component: () => import(/* webpackChunkName: "signup" */ '../views/Index.vue'),
    children: [
      {
        path: '/',
        name: 'signup-index',
        redirect: {
          name: 'signup-search'
        }
      },
      {
        path: 'search',
        name: 'signup-search',
        component: () => import(/* webpackChunkName: "signup" */ '../views/Search.vue')
      },
      {
        path: 'extra',
        name: 'signup-extra',
        component: () => import(/* webpackChunkName: "signup" */ '../views/Extra.vue')
      }
    ]
  }
]
