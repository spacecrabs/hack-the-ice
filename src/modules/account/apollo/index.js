import gql from 'graphql-tag'

export const USER = gql`
  query user($id: Int!)  {
    user: user_by_pk(id: $id) {
      id
      inn
      provide_access_to_credit_history
      reg_date
      reg_address
      deals {
        id
      }
      company_info {
        id
        ogrn
        company_name
        rating
        services
        list_of_contractors
        credit_history
        cash_flow
        verification_of_partners
      }
    }
  }
`
