export default [
  {
    path: '/account',
    component: () => import(/* webpackChunkName: "account" */ '../views/Index.vue'),
    children: [
      {
        path: '/',
        name: 'account-profile',
        component: () => import(/* webpackChunkName: "account" */ '../views/Profile.vue')
      }
    ]
  }
]
