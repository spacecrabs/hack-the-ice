const state = {
  userId: 1,
  isGuest: true
}

const getters = {
}

const actions = {
  onSetUserId: ({ commit }, id) => {
    commit('setUserId', id)
  },
  onSetUserStatus: ({ commit }, value) => {
    commit('setUserStatus', value)
  }
}

const mutations = {
  setUserId (state, id) {
    state.userId = id
  },
  setUserStatus (state, value) {
    state.isGuest = value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
