import gql from 'graphql-tag'

export const USER_LIST = gql`
  query userList($condition: user_bool_exp!) {
    userList: user(where: $condition) {
      id
      company_info {
        id
        company_name
      }
    }
  }
`

export const USER = gql`
  query userCatalog($id: Int!)  {
    user: user_by_pk(id: $id) {
      id
      inn
      provide_access_to_credit_history
      reg_date
      reg_address
      deals {
        id
      }
      company_info {
        cash_flow
        cash_flow
        company_name
        credit_history
        current_account_statement
        id
        list_of_contractors
        rating
        services
        verification_of_partners
        ogrn
      }
    }
  }
`
