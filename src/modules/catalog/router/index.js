export default [
  {
    path: '/catalog',
    component: () => import(/* webpackChunkName: "account" */ '../views/Index.vue'),
    children: [
      {
        path: '/',
        name: 'catalog-list',
        component: () => import(/* webpackChunkName: "account" */ '../views/List.vue')
      },
      {
        path: ':id',
        name: 'catalog-item',
        component: () => import(/* webpackChunkName: "account" */ '../views/Item.vue')
      }
    ]
  }
]
