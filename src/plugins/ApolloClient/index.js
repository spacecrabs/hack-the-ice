import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'

import { createHttpLink } from 'apollo-link-http'

// HTTP connection to the API
const link = createHttpLink({
  uri: 'https://hti.spacecrabs.ru/api/v1/graphql',
  headers: {
    'x-hasura-admin-secret': 'CgbBSChcaJLZrH25md29kh6mtF7ES6AtyScJ'
  }
})

// Cache implementation
const cache = new InMemoryCache({
  dataIdFromObject: object => object.key || null
})

// Create the apollo client
const apolloClient = new ApolloClient({
  link: link,
  cache,
  connectToDevTools: true
})

export default apolloClient
